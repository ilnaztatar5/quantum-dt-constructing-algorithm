import java.io.File
import java.util.*

/**************************************************
 ***************** Data Classes ********************
 ***************************************************/

/**
 * The tree representing class
 */
data class Node(
        val isLeaf: Boolean = false,
        val label: String = "",
        val children: Map<out Predicate, Node> = emptyMap(),
        val splitData: List<Data>? = null,
        val level: Int
)

/**
 * Attribute value class.
 * It is used to store training set values
 */
sealed class AttributeVal(val label: String) {
    class RV(label: String, var value: Double) : AttributeVal(label)
    class CV(label: String, var value: String) : AttributeVal(label)

    companion object {
        fun <T> generateAttrVal(attribute: Attribute, value: T): AttributeVal =
                when (attribute) {
                    is Attribute.RV -> RV(attribute.label, value as Double)
                    else -> CV(attribute.label, value as String)
                }

    }
}

/**
 * Predicate class.
 * It is used as a predicate to predict the class of new objects
 */
sealed class Predicate(val label: String) {
    class RV(label: String, val threshold: Double) : Predicate(label)
    class CV(label: String, val value: String) : Predicate(label)
}

/**
 * The training set object class
 */
data class Data(val data: Map<String, AttributeVal>, val classNumber: String)

/**
 * The attribute class
 */
sealed class Attribute(val label: String) {
    class RV(label: String, val l: Double, val r: Double) : Attribute(label)
    class CV(label: String, val values: List<String>) : Attribute(label)
    object DEFAULT : Attribute("DEFAULT")
}

/**
 * The processed value class.
 * It is used to store the data of processes attribute
 */
data class ProcessedValue(
        val maxVal: Double,
        val maxSplit: List<List<Data>>,
        val threshold: Double
)

/**************************************************
 ************** Generating Functions ***************
 **************************************************/

private val fileName = "abalone"
val attrs = generateAttrs()
val classes = generateClasses()
val data = generateData()

private fun generateAttrs(): List<Attribute> {
    val list = mutableListOf<Attribute>()

    with(Scanner(File("$fileName.attrs"))) {
        while (hasNext()) {
            val type = next()
            list.add(
                    if (type == "RV") Attribute.RV(next(), nextDouble(), nextDouble())
                    else {
                        val name = next()
                        val size = nextInt()
                        val vals = mutableListOf<String>()
                        repeat(size) {
                            vals.add(next())
                        }
                        Attribute.CV(name, vals)
                    })
        }
    }

    return list
}

private fun generateClasses(): List<String> {
    val list = mutableListOf<String>()

    with(Scanner(File("$fileName.cls"))) {
        while (hasNext()) {
            list.add(next())
        }
    }

    return list
}

private fun generateData(): List<Data> {
    val scanner = Scanner(File("$fileName.data"))
    val list = mutableListOf<Data>()

    while (scanner.hasNext()) {
        val map = mutableMapOf<String, AttributeVal>()
        attrs.forEach {
            if (it is Attribute.RV) {
                map[it.label] = AttributeVal.generateAttrVal(it, scanner.nextDouble())
            } else {
                map[it.label] = AttributeVal.generateAttrVal(it, scanner.next())
            }
        }
        list.add(
                Data(map, scanner.next())
        )
    }

    scanner.close()
    return list
}