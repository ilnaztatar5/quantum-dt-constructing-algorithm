import java.util.*
import kotlin.math.PI
import kotlin.math.log2
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

/**
 * Function of Quantum Tree Growing
 */
fun quantumTreeGrowing(x: List<Data>, level: Int, height: Int): Node {
    var t = Node(level = level)
    val needStop = needStop(x, level, height)

    if (needStop.second) {
        return t.copy(isLeaf = true, label = needStop.first, splitData = x)
    } else {
        val res = quantumChooseSplitRnd(x, height)

        if (res.first is Attribute.RV) {
            if (res.second.maxSplit.any { it.isEmpty() }) {
                val ns = needStop(res.second.maxSplit.first { it.isNotEmpty() }, level, height)
                return t.copy(isLeaf = true, label = ns.first)
            }
            val m = mapOf(
                    Predicate.RV(res.first.label, res.second.threshold) to quantumTreeGrowing(
                            res.second.maxSplit[0],
                            level + 1,
                            height
                    ),
                    Predicate.RV(res.first.label, res.second.threshold) to quantumTreeGrowing(
                            res.second.maxSplit[1],
                            level + 1,
                            height
                    )
            )
            t = t.copy(children = m, label = res.first.label)
        } else {
            val splits = x.sortedBy { (it.data[res.first.label] as AttributeVal.CV).value }.groupBy { (it.data[res.first.label] as AttributeVal.CV).value }
            if (splits.any { it.value.isEmpty() }) {
                val ns = needStop(x, level, height)
                return t.copy(isLeaf = true, label = ns.first)
            }
            val m = mutableMapOf<Predicate.CV, Node>()
            (res.first as Attribute.CV).values.forEach {
                splits[it]?.let { dt ->
                    m[Predicate.CV(res.first.label, it)] =
                            quantumTreeGrowing(
                                    dt,
                                    level + 1,
                                    height
                            )

                }
            }
            t = t.copy(children = m, label = res.first.label)
        }
    }

    return t
}

/**
 * Function for choosing attribute to construct new split.
 * This function is based on Durr-Hoyer minimum search algorithm
 * Note, that this algorithm compares all attributes with the selected one
 */
fun quantumChooseSplit(data: List<Data>, height: Int): Pair<Attribute, ProcessedValue> {
    val size = attrs.size
    val WH = generateWHGate(size)
    var resMax: ProcessedValue? = null
    var resIdx = 0
    val rCount = log2((attrs.size * 2.0.pow(height))).toInt()

    repeat(rCount) {
        var mIdx = Random.nextInt(size)
        var max = processAttr(attrs[mIdx], data)
        repeat((22.5 * sqrt(size.toDouble()) + log2(size.toFloat()).pow(2)).toInt()) {
            var state = Array(attrs.size) {
                if (it == 0)
                    1.0
                else 0.0
            }
            var stateFound = false
            state = state.applyGate(WH)
            var repCount = 1
            while (!stateFound) {
                repCount *= 2
                val Uf = generateUf(size) { i ->
                    val value = processAttr(attrs[i], data).maxVal
                    return@generateUf value >= max.maxVal
                }
                repeat(repCount) {//(sqrt(size.toDouble()) * PI / 4).toInt()) {
                    state = state.applyGate(Uf)
                    state = state.applyInversion()
                }

                var mVal = 1.0 / size
                state.forEachIndexed { index, fl ->
                    if (fl.pow(2) > mVal) {
                        mIdx = index
                        mVal = fl
                        stateFound = true
                    }
                }

                if (size <= repCount && !stateFound) {
                    mIdx = Random.nextInt(size)
                    max = processAttr(attrs[mIdx], data)
                    state = Array(attrs.size) {
                        if (it == 0)
                            1.0
                        else 0.0
                    }
                    state = state.applyGate(WH)
                    repCount = 1
                }
            }

            max = processAttr(attrs[mIdx], data)
        }
        if (max.maxVal > resMax?.maxVal ?: 0.0) {
            resMax = max
            resIdx = mIdx
        }
    }

    return attrs[resIdx] to resMax!!
}

/**
 * Function for choosing attribute to construct new split.
 * This function is based on Durr-Hoyer minimum search algorithm
 * Note, that this algorithm selects attribute randomly to compare
 * with selected attribute
 */
fun quantumChooseSplitRnd(data: List<Data>, height: Int): Pair<Attribute, ProcessedValue> {
    val size = attrs.size
    val WH = generateWHGate(size)

    var resMax: ProcessedValue? = null
    var resIdx = 0
    val rCount = log2((attrs.size * 2.0.pow(height))).toInt()

    repeat(rCount) {
        var mIdx = Random.nextInt(size)
        var max = processAttr(attrs[mIdx], data)
        repeat(23) {
            var state = Array(attrs.size) {
                if (it == 0)
                    1.0
                else 0.0
            }

            var stateFound = false
            state = state.applyGate(WH)

            val Uf = generateUfRandom(size) { i ->
                val value = processAttr(attrs[i], data).maxVal
                return@generateUfRandom value >= max.maxVal
            }

            repeat((sqrt(size.toDouble()) * PI / 4).toInt()) {
                state = state.applyGate(Uf)
                state = state.applyInversion()
            }

            var mVal = 1.0 / size
            state.forEachIndexed { index, fl ->
                if (fl.pow(2) > mVal && !stateFound) {
                    mIdx = index
                    mVal = fl
                    stateFound = true
                }
            }

            if (stateFound) {
                max = processAttr(attrs[mIdx], data)
            }
        }
        if (max.maxVal > resMax?.maxVal ?: 0.0) {
            resMax = max
            resIdx = mIdx
        }
    }

    return attrs[resIdx] to resMax!!
}

/* * * * * * * * * * * * *
 * * * Quantum Gates * * *
 * * * * * * * * * * * * */

/**
 * This function prints the current gate
 */
fun Array<Array<Double>>.printMe() {
    forEach {
        it.forEach { data ->
            print("${formatter(data)} ")
        }
        println()
    }
}

/**
 * Function for generating oracle gate, note, that the size of gate is equals to attribute size.
 */
fun generateUf(size: Int, func: (Int) -> (Boolean)): Array<Array<Double>> {
    return Array(size) { i ->
        Array(size) { j ->
            if (i == j) {
                val pow = if (func(i)) 1 else 0
                (-1.0).pow(pow)
            } else {
                0.0
            }
        }
    }
}

/**
 * Function for generating oracle gate, note, that the size of gate is equals to attribute size.
 */
fun generateUfRandom(size: Int, func: (Int) -> (Boolean)): Array<Array<Double>> {
    val arr = Array(size) { i ->
        Array(size) { j ->
            if (i == j) {
                1.0
            } else {
                0.0
            }
        }
    }

    val rnd = Random.nextInt(size)
    if (func(rnd)) {
        arr[rnd][rnd] = -1.0
    }
    return arr
}

/**
 * This function constructs the inversion gate
 */
fun inversionGate(size: Int) =
        Array(size) { i ->
            Array(size) { j ->
                2.0 / size - if (i == j) 1.0 else 0.0
            }
        }

/**
 * This function generates The Walsh-Hadamard gate's matrix
 * An argument is the size of WH matrix
 */
fun generateWHGate(size: Int) = Array(size) { i ->
    Array(size) { j ->
        1 / (2.0).pow(log2(size.toDouble()) / 2) * (-1.0).pow((i and j).countOneBits())
    }
}

/**
 * This function implements and tests the Grover algorithm
 */
fun runGrover(size: Int) {
    var state = Array(size) {
        if (it == 0)
            1.0
        else 0.0
    }

    state = state.applyGate(generateWHGate(size))

    val M = Random.nextInt(size)
    println(M)

    val randomIndexes = Array(M) {
        Random.nextInt(size)
    }

    val uniqList = randomIndexes.distinct()

    println(uniqList)

    val Uf = generateUf(size) { i ->
        return@generateUf uniqList.contains(i)
    }

    Uf.printMe()

    repeat((sqrt(size.toFloat() / uniqList.size) * PI / 4).toInt()) {
        state = state.applyGate(Uf)
        state = state.applyInversion()
    }

    state.printMe()

    val sumSq = state.sumByDouble { it.pow(2) }

    println(sumSq)
}

/* * * * * * * * * * * * * *
 * * * State Functions * * *
 * * * * * * * * * * * * * */

val formatter = { value: Double -> Formatter().format("%.2f", value).toString() }

/**
 * This function prints the current state
 */
fun Array<Double>.printMe() {
    forEach {
        print("${formatter(it)} ")
    }
    println()
}

/**
 * This function applies inversion operation without
 * using inversion gate
 */
fun Array<Double>.applyInversion(): Array<Double> {
    val res = Array(size) { 0.0 }

    val mean = this.sum() / size

    forEachIndexed { index, d ->
        res.set(index, 2 * mean - get(index))
    }

    return res
}

/**
 * This function apples the gate to current state
 */
fun Array<Double>.applyGate(gate: Array<Array<Double>>): Array<Double> {
    val res = Array(size) { 0.0 }
    gate.forEachIndexed { i, value ->
        var coord = 0.0
        value.forEachIndexed { j, data ->
            coord += data * get(j)
        }
        res[i] = coord
    }

    return res
}
