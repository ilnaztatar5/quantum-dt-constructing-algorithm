import java.util.*
import kotlin.math.log2

/**
 * Function of Tree Growing
 */
private fun treeGrowing(x: List<Data>, level: Int, height: Int): Node {
    var node = Node(level = level)

    val needStop = needStop(x, level, height)

    if (needStop.second) {
        return node.copy(isLeaf = true, label = needStop.first, splitData = x)
    } else {
        val res = chooseSplit(x)

        if (res.first is Attribute.RV) {
            if (res.second.maxSplit[0].isEmpty() || res.second.maxSplit[1].isEmpty()) {
                val splData = res.second.maxSplit.first { it.isNotEmpty() }
                val ns = needStop(splData, level, height)
                return node.copy(isLeaf = true, label = ns.first, splitData = splData)
            }
            val m = mapOf(
                    Predicate.RV(res.first.label, res.second.threshold) to treeGrowing(
                            res.second.maxSplit[0],
                            level + 1,
                            height
                    ),
                    Predicate.RV(res.first.label, res.second.threshold) to treeGrowing(
                            res.second.maxSplit[1],
                            level + 1,
                            height
                    )
            )
            node = node.copy(children = m, label = res.first.label)
        } else {
            val splits = x.sortedBy { (it.data[res.first.label] as AttributeVal.CV).value }.groupBy { (it.data[res.first.label] as AttributeVal.CV).value }
            if (splits.any { it.value.isEmpty() }) {
                val ns = needStop(x, level, height)
                return node.copy(isLeaf = true, label = ns.first)
            }
            val m = mutableMapOf<Predicate.CV, Node>()
            (res.first as Attribute.CV).values.forEach {
                splits[it]?.let { dt ->
                    m[Predicate.CV(res.first.label, it)] =
                            treeGrowing(dt, level + 1, height)
                }
            }
            node = node.copy(children = m, label = res.first.label)
        }
    }

    return node
}

/**
 * Function for check of necessity to stop of splitting the current node
 */
fun needStop(x: List<Data>, level: Int, height: Int): Pair<String, Boolean> {
    val classesCount = mutableMapOf<String, Int>()
    if (x.isEmpty()) {
        "None" to true
    }
    x.forEach { classesCount[it.classNumber] = (classesCount[it.classNumber] ?: 0) + 1 }

    val cn = classesCount.entries.sortedWith(compareBy {
        it.value
    }).lastOrNull()?.key ?: "None"

    if (level >= height) return cn to true

    return cn to (classesCount.size == 1 || x.size < 20)
}

/**
 * Function for choosing attribute to construct new split
 */
private fun chooseSplit(x: List<Data>): Pair<Attribute, ProcessedValue> {
    var mAtr: Attribute = Attribute.DEFAULT
    var mVal = ProcessedValue(-1.0, emptyList(), -1.0)
    attrs.forEach {
        val pv = processAttr(it, x)

        if (pv.maxVal > mVal.maxVal) {
            mVal = pv
            mAtr = it
        }
    }

    return mAtr to mVal
}

/**
 * Function for attribute processing
 */
fun processAttr(attr: Attribute, x: List<Data>): ProcessedValue {
    return if (attr is Attribute.CV) {
        processCV(attr, x)
    } else {
        processRV(attr as Attribute.RV, x)
        //processRVNaive(attr as Attribute.RV, x)
    }
}

/**
 * Algorithm of processing real-valued attributes
 */
private fun processRV(attr: Attribute.RV, x: List<Data>): ProcessedValue {
    val z = x.size

    //precounted classes
    val pC = mutableMapOf<String, Array<Int>>()
    val sC = mutableMapOf<String, Array<Int>>()

    val pF = Array(z) { 0.0 }
    val sF = Array(z) { 0.0 }

    classes.forEach {
        pC[it] = Array(z) { 0 }
        sC[it] = Array(z) { 0 }
    }

    val sx = x.sortedBy { (it.data[attr.label] as AttributeVal.RV).value }

    sx.forEachIndexed { idx, data ->
        val j = data.classNumber
        pC.keys.forEach {
            pC[it]!![idx] = pC[it]!!.getOrElse(idx - 1) { 0 } + (if (it == j) 1 else 0)
        }
    }

    sx.reversed().forEachIndexed { idx, data ->
        val j = data.classNumber
        sC.keys.forEach {
            sC[it]!![idx] = sC[it]!!.getOrElse(idx - 1) { 0 } + (if (it == j) 1 else 0)
        }
    }

    sx.forEachIndexed { idx, data ->
        var pft = 0.0
        var sft = 0.0
        pC.keys.forEach { j ->
            val pRF = pC[j]!!.getOrElse(idx) { 0 }.toDouble() / (idx + 1)
            if (pRF != 0.0)
                pft += pRF * log2(pRF)

            val sRF = sC[j]!!.getOrElse(idx) { 0 }.toDouble() / (idx + 1)
            if (sRF != 0.0)
                sft += sRF * log2(sRF)
        }

        pF[idx] = -pft
        sF[z - (idx + 1)] = -sft
    }

    var maxGain = 0.0
    var maxThreshold = 0.0

    sx.forEachIndexed { index, data ->

        val iL = (index + 1).toDouble() / z * pF[index]
        val iR = (z - (index + 1)).toDouble() / z * sF.getOrElse(index + 1) { 0.0 }

        val gain = pF[sx.size - 1] - iL - iR
        //println("gain = ${gain}, ${pF[sx.size - 1]}, ${iL}, ${iR}")
        if (gain > maxGain) {
            maxGain = gain

            maxThreshold = (((sx.getOrNull(index)?.data?.get(attr.label) as? AttributeVal.RV)?.value
                    ?: 0.0) + ((sx.getOrNull(index + 1)?.data?.get(attr.label) as? AttributeVal.RV)?.value
                    ?: 0.0)) / 2.toDouble()
        }
    }

    val map = sx.groupBy {
        (it.data[attr.label] as AttributeVal.RV).value <= maxThreshold
    }

    //println("Selected TH = $mt, max = $max")

    val split = listOf(map[true] ?: emptyList(), map[false] ?: emptyList())
    return ProcessedValue(maxGain, split, maxThreshold)
}

/**
 * Naive algorithm of processing real-valued attributes
 */
private fun processRVNaive(attr: Attribute.RV, x: List<Data>): ProcessedValue {
    val pC = calcClassCountForData(x)

    val sx = x.sortedBy { (it.data[attr.label] as AttributeVal.RV).value }

    val impurity = calcImpurity(pC, x.size)

    var max = 0.0
    var mt = 0.0

    sx.forEachIndexed { index, data ->

        val left = sx.subList(0, index)
        val right = sx.subList(index, sx.size)
        val pCLeft = calcClassCountForData(left)
        val pCRight = calcClassCountForData(right)

        val iL = left.size.toDouble() / sx.size * calcImpurity(pCLeft, left.size)
        val iR = right.size.toDouble() / sx.size * calcImpurity(pCRight, right.size)

        val gain = (impurity - iL - iR)

        if (gain > max) {
            max = gain

            val lItem = (left.lastOrNull()?.data?.get(attr.label) as? AttributeVal.RV)?.value ?: 0.0
            val rItem = (right.firstOrNull()?.data?.get(attr.label) as? AttributeVal.RV)?.value ?: 0.0

            mt = (lItem + rItem) / 2.toDouble()
        }
    }

    val map = sx.groupBy {
        (it.data[attr.label] as AttributeVal.RV).value <= mt
    }

    val split = listOf(map[true] ?: emptyList(), map[false] ?: emptyList())
    return ProcessedValue(max, split, mt)
}

/**
 * The function for calculating an impurity value for naive algorithm
 */
private fun calcImpurity(mapClasses: Map<String, Int>, dataSize: Int): Double {
    var impurity = 0.0

    mapClasses.keys.forEach {
        val rf = mapClasses[it]!!.toDouble() / dataSize
        impurity -= if (rf > 0) rf * log2(rf) else 0.0
    }
    return impurity
}

/**
 * The function for calculating objects count distributed by classes for naive algorithm
 */
private fun calcClassCountForData(sx: List<Data>): MutableMap<String, Int> {
    val pC = mutableMapOf<String, Int>()

    classes.forEach {
        pC[it] = 0
    }

    sx.forEach { data ->
        val j = data.classNumber
        pC.keys.forEach {
            if (it == j) {
                pC[j] = pC[j]!! + 1
            }
        }
    }

    return pC
}

/**
 * Procedure or processing CV-attributes
 */
private fun processCV(attr: Attribute.CV, x: List<Data>): ProcessedValue {
    val groupedXByClasses = x.groupBy {
        it.classNumber
    }.mapValues { it.value.size }

    val impBefore = calcImpurity(groupedXByClasses, x.size)

    val groupedXByAttrVal = x.groupBy {
        (it.data[attr.label] as AttributeVal.CV).value
    }.mapValues {
        it.value.groupBy {
            it.classNumber
        }.mapValues { it.value.size }
    }

    var impAfter = 0.0

    groupedXByAttrVal.forEach { s, map ->
        val size = map.values.sum()
        impAfter += size.toDouble() / x.size * calcImpurity(map, size)
    }

    val gain = impBefore - impAfter

    return ProcessedValue(gain, emptyList(), -1.0)
}

fun main() {
    val heights = arrayOf(3, 5, 7, 10)
    Locale.setDefault(Locale.ENGLISH)

    for (height in heights) {
        println("Classical version")
        val tree = treeGrowing(data, 0, height)
        println("Done")
        println("Quantum version")

        val qTree = quantumTreeGrowing(data, 0, height)
        val all = getTreeNodeCount(tree)
        val nonEq = getNonEqualNodes(tree, qTree)

        val prb = (all - nonEq) / all.toDouble()
        println("Done")
        println("Equality for h=$height ~ $prb")
    }
    println("Finish")
}

/**
 * The function for printing the tree
 */
fun printTree(tree: Node) {
    val lst = mutableListOf(tree)

    var level = tree.level

    while (lst.isNotEmpty()) {
        val n = lst.removeFirst()

        if (level != n.level) {
            println()
            level = n.level
        }
        print("${n.label} ")

        n.children.forEach {
            lst.add(it.value)
        }
    }
    println()
}

/**
 * The function for calculating the not equal nodes count of the tree
 */
fun getNonEqualNodes(t1: Node, t2: Node): Int {
    var notEquals = 0
    if (t1.label == t2.label) {
        if (t1.isLeaf) {
            return 0
        } else {
            var i = 0
            while (i < t1.children.size) {
                notEquals += getNonEqualNodes(t1.children.values.elementAt(i), t2.children.values.elementAt(i))
                i++
            }
        }
    } else {
        notEquals += getTreeNodeCount(t1)
    }
    return notEquals
}

/**
 * The function for calculating the nodes count of the tree
 */
fun getTreeNodeCount(t: Node): Int {
    var nodeCount = 0

    return if (t.isLeaf) 1
    else {
        t.children.entries.forEach {
            nodeCount += getTreeNodeCount(it.value)
        }
        nodeCount
    }
}